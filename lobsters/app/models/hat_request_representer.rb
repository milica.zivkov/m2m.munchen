require 'roar/json'

class HatRequestRepresenter < OpenStruct
  include Roar::JSON
  property :subject
  property :object
  collection :evidence

  def addEvidence(text, link)
    evidence = [{:text => text}, {:link => link}]
  end
end

