# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application

# https://github.com/openzipkin/zipkin-ruby#sending-traces-on-incoming-requests
zipkin_config = {
    service_name: 'lobsters',
    service_port: 3000,
    sample_rate: 1,
    sampled_as_boolean: false,
    log_tracing: true,
    json_api_host: "http://#{Lobsters::Application.config.tracing_host}:9411/api/v1/spans"
}

use ZipkinTracer::RackHandler, zipkin_config

run Rails.application

