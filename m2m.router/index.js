const express = require('express');
const proxy = require('http-proxy-middleware')
var app = express()

var proxyOptions = {
    changeOrigin: true,
    target: process.env.LOBSTERS_DNS_NAME,
    router: {
        '/modlog': process.env.MODLOG_DNS_NAME
    }
}
app.use(proxy('/', proxyOptions))
    .listen(1972, () => console.log('Listening on port 1972'))