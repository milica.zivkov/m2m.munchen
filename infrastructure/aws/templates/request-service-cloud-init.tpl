#cloud-config
write_files:
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/telegraf.conf
    content: "${telegraf_conf_content}"
    permissions: '0644'
-   encoding: b64
    path:  /home/ubuntu/app/application-production.properties
    content: "${database_yml_content}"
    permissions: '0644'

package_upgrade: false

packages:
  - awscli
  - openjdk-8-jdk

runcmd:
- wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
- sudo dpkg -i telegraf_1.6.3-1_amd64.deb 
- mkdir -p /home/ubuntu/app
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${request_bucket}/${request_code}" /home/ubuntu/app/
- cd /home/ubuntu/app && java -jar "/home/ubuntu/app/${request_code}" --spring.profiles.active=production