InfluxDB::Rails.configure do |config|
  config.influxdb_database = "munchen"
  config.influxdb_username = ""
  config.influxdb_password = ""
  config.influxdb_hosts    = ["${influx_service_url}"]
  config.influxdb_port     = 8086
end