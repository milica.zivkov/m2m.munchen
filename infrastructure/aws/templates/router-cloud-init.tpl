#cloud-config

package_upgrade: false

packages:
  - awscli
  - nodejs

runcmd:
- mkdir -p /home/ubuntu/app
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_bucket}/${router_code}" /var/lib/router/m2m.router.tgz
- chown -R ubuntu /home/ubuntu/router
- su -l -c "tar xzf /var/lib/router/m2m.router.tgz" ubuntu
- su -l -c "cd router && LOBSTERS_DNS_NAME=${lobsters_dns_name} MODLOG_DNS_NAME=${modlog_dns_name} node index.js" ubuntu