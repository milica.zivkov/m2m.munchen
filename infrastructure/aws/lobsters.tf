variable "port-lobsters" {
  default = "3000"
}

variable "rails_secret_key" {
  description = "A long secret key which is used to verify the integrity of signed cookies"
  default     = "9699266eeb10d15584e94e349ab1d82507ddb5afbecb24338fdfb738e4c2a7ed19869b159ed1353f937f6e02e0ff9a1de8b0b32331462506ec45b77ac6fe46b0"
}

resource "aws_lb" "lb-lobsters" {
  name               = "lb-lobsters-${var.student_id}"
  security_groups    = ["${aws_security_group.sg-fe.id}"]
  internal           = false
  load_balancer_type = "application"

  subnets = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  tags {
    Name = "lb-lobsters-${var.student_id}"
  }
}

resource "aws_lb_target_group" "tg-lobsters" {
  name        = "tg-lobsters-${var.student_id}"
  port        = "${var.port-lobsters}"
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.vpc.id}"
  target_type = "instance"
}

resource "aws_lb_listener" "listen-lobsters" {
  load_balancer_arn = "${aws_lb.lb-lobsters.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.tg-lobsters.arn}"
    type             = "forward"
  }
}

data "template_file" "lobsters-database-yml" {
  template = "${file("templates/lobsters-database.yml")}"

  vars {
    database_host            = "${aws_db_instance.db-lobsters.address}"
    database_name            = "lobsters_production"
    database_master_user     = "${var.database_username}"
    database_master_password = "${var.database_password}"
  }
}

data "template_file" "lobsters-production-rb" {
  template = "${file("templates/lobsters-production.rb")}"

  vars {
    request_service_lb = "${aws_lb.lb-request.dns_name}"
    tracing_host = "${var.zipkin_hostname}"
  }
}

data "template_file" "lobsters-influxdb-rails-rb" {
  template = "${file("templates/lobsters-influxdb-rails.rb")}"

  vars {
    influx_service_url = "ec2-18-184-49-169.eu-central-1.compute.amazonaws.com"
  }
}

data "template_file" "cloud-init-lobsters" {
  template = "${file("${path.module}/templates/lobsters-cloud-init.tpl")}"

  vars {
    lobsters_bucket       = "${var.s3_bucket}"
    lobsters_code         = "lobsters.tgz"
    aws_access_key_id     = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    rails_secret_key      = "${var.rails_secret_key}"
    tracing_host          = "${var.zipkin_hostname}"

    database_yml_content      = "${base64encode("${data.template_file.lobsters-database-yml.rendered}")}"
    production_rb_content     = "${base64encode("${data.template_file.lobsters-production-rb.rendered}")}"
    influxdb_rails_rb_content     = "${base64encode("${data.template_file.lobsters-influxdb-rails-rb.rendered}")}"
    telegraf_conf_content = "${base64encode("${data.template_file.telegraf-conf.rendered}")}"
  }
}

resource "aws_launch_configuration" "lc-lobsters" {
  name_prefix                 = "lc-lobsters-${var.student_id}-"
  image_id                    = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.keypair.key_name}"
  security_groups             = ["${aws_security_group.sg-apps.id}"]
  associate_public_ip_address = true

  user_data = "${data.template_file.cloud-init-lobsters.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg-lobsters" {
  name                 = "asg-lobsters-${var.student_id}"
  launch_configuration = "${aws_launch_configuration.lc-lobsters.id}"

  vpc_zone_identifier = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  min_size     = 2
  max_size     = 2
  force_delete = true

  target_group_arns = ["${aws_lb_target_group.tg-lobsters.arn}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Owner"
    value               = "student-${var.student_id}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "asg-lobsters-${var.student_id}"
    propagate_at_launch = false
  }
}

output "lobsters-dns-name" {
  value = "${aws_lb.lb-lobsters.dns_name}"
}

