resource "aws_db_instance" "db-lobsters" {
  allocated_storage      = 10
  storage_type           = "gp2"
  engine                 = "mariadb"
  engine_version         = "10.0.24"
  instance_class         = "db.t2.micro"
  name                   = "lobsters_production"
  username               = "${var.database_username}"
  password               = "${var.database_password}"
  db_subnet_group_name   = "${aws_db_subnet_group.subg-db.name}"
  skip_final_snapshot    = true
  vpc_security_group_ids = ["${aws_security_group.sg-db.id}"]

  tags {
    Name = "db-lobsters-${var.student_id}"
  }
}


output "lobsters-db" {
  value = "${aws_db_instance.db-lobsters.address}"
}