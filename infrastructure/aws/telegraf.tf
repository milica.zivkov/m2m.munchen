data "template_file" "telegraf-conf" {
  template = "${file("templates/telegraf-production.conf")}"

  vars {
    telegraf_url = "http://ec2-18-184-49-169.eu-central-1.compute.amazonaws.com:8086"
    influx_db = "munchen"
  }
}