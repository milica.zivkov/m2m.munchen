package com.n6consulting.m2m.request;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Request {

  public boolean isOpen() {
    return status == Status.pending;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  enum Status {
    pending, approved, rejected
  }

  @Id
  @GeneratedValue
  private Long requestId;

  private String subject;
  private String object;
  private Status status = Status.pending;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "request", fetch = FetchType.EAGER)
  private Set<Evidence> evidence = new HashSet<>();

  @OneToOne(cascade = CascadeType.ALL)
  private Notarization notarization;

  public Long getRequestId() {
    return requestId;
  }

  public String getSubject() {
    return subject;
  }

  public String getObject() {
    return object;
  }

  public Set<Evidence> getEvidence() {
    return evidence;
  }

  public Status getStatus() {
    return status;
  }

  public Request notarize(Notarization notarization) {
    this.notarization = notarization;
    return this;
  }

}

