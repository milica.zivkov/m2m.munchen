package com.n6consulting.m2m.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
@RequestMapping("/request")
public class RequestController {
  @Autowired
  private RequestRepository requestRepository;

  @RequestMapping(method = RequestMethod.GET)
  Resources<Resource<Request>> index() {
    final Iterable<Request> all = requestRepository.findAll();

    List<Resource<Request>> allRes =
            StreamSupport.stream(all.spliterator(), false)
                    .map(RequestController::createResource)
                    .collect(Collectors.toList());

    return new Resources<>(allRes);
  }

  @RequestMapping(method = RequestMethod.POST)
  Resource<Request> add(@RequestBody Request input) {
    for (Evidence e : input.getEvidence()) {
      e.setRequest(input);
    }
    return createResource(requestRepository.save(input));
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  ResponseEntity<Resource<Request>> getById(@PathVariable Long id) {
    Optional<Request> request = requestRepository.findById(id);

    return request.map(RequestController::createResource)
            .flatMap(res -> Optional.of(ResponseEntity.ok(res)))
            .orElse(ResponseEntity.notFound().build());
  }

  @RequestMapping(path = "status", method = RequestMethod.GET)
  Resources<Resource<Request>> findByStatus(@RequestParam("status") Request.Status param) {
    Iterable<Request> requests = requestRepository.findByStatus(param);

    List<Resource<Request>> resources =
            StreamSupport.stream(requests.spliterator(), false)
                    .map(RequestController::createResource)
                    .collect(Collectors.toList());

    return new Resources<>(resources);
  }

  @RequestMapping(path = "/{id}/{action}", method = RequestMethod.POST)
  ResponseEntity<Resource<Request>> action(@PathVariable(name = "id") Long id,
                                           @PathVariable("action") Action action,
                                           @RequestBody Notarization notarization) {
    Optional<Request> request = requestRepository.findById(id);

    return request.filter(Request::isOpen)
            .map(action::doTo)
            .map(r -> r.notarize(notarization))
            .map(requestRepository::save)
            .map(RequestController::createResource)
            .flatMap(r -> Optional.of(ResponseEntity.ok(r)))
            .orElse(ResponseEntity.notFound().build());
  }

  static Resource<Request> createResource(Request request) {
    Resource<Request> resource = new Resource<>(request);
    Link linkGetRequest = linkTo(methodOn(RequestController.class).getById(request.getRequestId())).withSelfRel();
    resource.add(linkGetRequest);
    resource.add(makeActionLinks(request));
    return resource;
  }

  private static Iterable<Link> makeActionLinks(Request request) {
    if (!request.isOpen()) {
      return Collections.emptyList();
    }
    return Arrays.stream(Action.values())
            .map(action -> actionLink(action, request))
            .collect(Collectors.toList());
  }

  private static Link actionLink(Action action, Request request) {
    return linkTo(methodOn(RequestController.class).action(request.getRequestId(), action, null)).withRel(action.name());
  }

  enum Action implements Identifiable<String> {
    approve(Request.Status.approved),
    reject(Request.Status.rejected);

    private Request.Status targetStatus;

    Action(Request.Status status) {
      this.targetStatus = status;
    }

    @Override
    public String getId() {
      return this.name();
    }

    public Request doTo(Request request) {
      request.setStatus(targetStatus);
      return request;
    }
  }


}
