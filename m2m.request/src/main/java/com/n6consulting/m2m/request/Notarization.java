package com.n6consulting.m2m.request;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Notarization {
  @Id
  @GeneratedValue
  private Long notarizationId;

  private String principal;
  private String reason;

  public Long getNotarizationId() {
    return notarizationId;
  }

  public String getPrincipal() {
    return principal;
  }

  public String getReason() {
    return reason;
  }
}

